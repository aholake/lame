package com.aholake.lame.controller;

import com.aholake.lame.model.User;
import com.aholake.lame.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("")
    public Iterable<User> getAll() {
        return this.userService.findAll();
    }
}
