package com.aholake.lame.service;

import com.aholake.lame.model.User;
import com.aholake.lame.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    public void init() {
        User user1 = User.builder()
                .id(1L)
                .name("Michael Jackson")
                .email("jackson@gmail.com")
                .build();
        User user2 = User.builder()
                .id(2L)
                .name("Dan Nguyen")
                .email("dannguyen@gmail.com")
                .build();

        this.userRepository.saveAll(Arrays.asList(user1,user2));
    }

    public Iterable<User> findAll() {
        return this.userRepository.findAll();
    }
}
