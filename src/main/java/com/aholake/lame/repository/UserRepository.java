package com.aholake.lame.repository;

import com.aholake.lame.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
