//package com.aholake.lame.controller;
//
//import com.aholake.lame.service.UserService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import java.util.Collections;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest(UserController.class)
//public class UserControllerTest {
//
//    @Autowired
//    private MockMvc mockmvc;
//
//    @MockBean
//    private UserService userService;
//
//    @Test
//    public void whenFindAllUsers_thenReturnJsonArray() throws Exception {
//        Mockito.when(this.userService.findAll()).thenReturn(Collections.emptyList());
//
//        MvcResult mvcResult = this.mockmvc.perform(MockMvcRequestBuilders.get("/users")
//                .contentType(MediaType.APPLICATION_JSON_VALUE))
//                .andReturn();
//    }
//}
