FROM pwittchen/alpine-java11
MAINTAINER Loc Vo <nluit.tanloc@gmail.com>
WORKDIR /lame
RUN mkdir ./app
RUN mkdir ./log
RUN touch ./app/lame.log
